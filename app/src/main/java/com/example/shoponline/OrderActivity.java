package com.example.shoponline;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.shoponline.adapter.ChartAdapter;
import com.example.shoponline.adapter.OrderAdapter;
import com.example.shoponline.model.AccountModel;
import com.example.shoponline.model.OrderModel;
import com.example.shoponline.model.ProductModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class OrderActivity extends AppCompatActivity {
    DatabaseReference database;
    RecyclerView rvChart;
    ArrayList<OrderModel> list = new ArrayList<>();
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        ivBack = findViewById(R.id.ivBack);
        rvChart = findViewById(R.id.rvChart);
        rvChart.setHasFixedSize(true);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference getRefenence = getDatabase.getReference();
        if (user != null){
            getRefenence.child("account").child(user.getUid()).addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    AccountModel accountModel = dataSnapshot.getValue(AccountModel.class);
                    if (accountModel.getType().equals("admin")){
                        database = FirebaseDatabase.getInstance().getReference();
                        database.child("order").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                list.clear();
                                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                                    OrderModel orderModel = noteDataSnapshot.getValue(OrderModel.class);
                                    orderModel.setKey(noteDataSnapshot.getKey());
                                    list.add(orderModel);
                                }
                                showRecyclerList();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
                            }
                        });
                    }else{
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        database = FirebaseDatabase.getInstance().getReference();
                        database.child("order").orderByChild("uid").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                list.clear();
                                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()){
                                    OrderModel orderModel = noteDataSnapshot.getValue(OrderModel.class);
                                    orderModel.setKey(noteDataSnapshot.getKey());
                                    list.add(orderModel);
                                }
                                showRecyclerList();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                System.out.println(databaseError.getDetails()+" "+databaseError.getMessage());
                            }
                        });
                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }

    private void showRecyclerList(){
        Collections.reverse(list);
        rvChart.setLayoutManager(new LinearLayoutManager(OrderActivity.this));
        OrderAdapter orderAdapter = new OrderAdapter(list);
        rvChart.setAdapter(orderAdapter);
    }
}
