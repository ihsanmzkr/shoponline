package com.example.shoponline.model;

public class AccountModel {
    String alamat, no_hp, type, pict, nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPict() {
        return pict;
    }

    public void setPict(String pict) {
        this.pict = pict;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public AccountModel(String alamat, String no_hp, String type, String pict, String nama) {
        this.alamat = alamat;
        this.no_hp = no_hp;
        this.type = type;
        this.pict = pict;
        this.nama = nama;
    }

    public AccountModel (){}
}
